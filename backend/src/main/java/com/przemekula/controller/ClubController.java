package com.przemekula.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.przemekula.model.Club;
import com.przemekula.model.dto.ClubDto;
import com.przemekula.service.ClubService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ClubController {

	@Autowired
	private ClubService clubService;

	@GetMapping("/clubs")
	public List<ClubDto> getAllClubs() {
		return clubService.getAllClubs();
	}

	// git
	@GetMapping("/clubs/{id}")
	public ClubDto getClubById(@PathVariable Long id) {
		return new ClubDto(clubService.getClubById(id));
	}

	// git
	@PostMapping("/clubs")
	public Club createClub(@Valid @RequestBody Club club) {
		return clubService.createClub(club);
	}

	// git
	@PutMapping("/clubs/{id}")
	public Club updateClub(@PathVariable Long id, @Valid @RequestBody Club clubUpdated) {
		return clubService.updateClub(id, clubUpdated);
	}

	// git
	@DeleteMapping("/clubs/{id}")
	public String deleteClub(@PathVariable Long id) {
		return clubService.deleteClub(id);
	}
	
	@PutMapping("/clubs/{clubId}/transfer/{playerId}/to/{newClubId}")
	public ClubDto transferPlayer(@PathVariable Long clubId,@PathVariable Long playerId,@PathVariable Long newClubId) {
		return new ClubDto(clubService.transferPlayer(clubId, playerId, newClubId));
	}
	
}