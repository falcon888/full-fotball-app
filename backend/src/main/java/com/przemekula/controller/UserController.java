package com.przemekula.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.przemekula.model.User;
import com.przemekula.model.dto.UserDto;
//import com.przemekula.service.RoleService;
import com.przemekula.service.UserService;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RestControllerAdvice
public class UserController {

    @Autowired
    private UserService userService;

//    private RoleService roleService;
    
    //@Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value="/users", method = RequestMethod.GET)
    public List<User> listUser(){
        return userService.findAll();
    }

    //@Secured("ROLE_USER")
    @PreAuthorize("hasRole('USER')") // czyli User ma dostep :)
    ////@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public User getOne(@PathVariable(value = "id") Long id){
        return userService.findById(id);
    }


    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public User saveUser(@RequestBody UserDto user){
//    	
//       User createdUser = userService.save(user);
//       System.out.println();
//       User newUser = userService.findById(createdUser.getId());
//        return userService.setOnStartUserAuthority(newUser);
    	
    	return userService.save(user);
   
    }

    @RequestMapping(value = "/users/userAuth/{id}", method = RequestMethod.GET)
    public User setUserAuthority(@PathVariable(value = "id") Long id){
         User user = userService.findById(id);
         return userService.setUserAuthority(user);
    }

}