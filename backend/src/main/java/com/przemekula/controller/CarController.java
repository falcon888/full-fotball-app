package com.przemekula.controller;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.przemekula.model.Car;
import com.przemekula.service.CarService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class CarController {

	@Autowired
	CarService carService;
	
	
	private Direction getDirection(int number) {
		
		if(number==1)
			return Direction.ASC;
		else
		return Direction.DESC;
	}

	@GetMapping("/cars")
	public List<Car> getAllCars(@RequestParam int pages,@RequestParam  int size ,@RequestParam  String sortField ,@RequestParam  int sortOrder) {
System.out.println("lalalala");
		return carService.getAllCars(pages, size, sortField, getDirection(sortOrder));
	}

	@PostMapping("/cars")
	public Car createCar(@Valid @RequestBody Car car) {
		return carService.createCar(car);
	}

	@PutMapping("/cars/{id}")
	public Car updateCar(@PathVariable Long id, @Valid @RequestBody Car carUpdated) {

		return carService.updateCar(id, carUpdated);
	}

	@DeleteMapping("/cars/{id}")
	public String deleteCar(@PathVariable Long id) {
		return carService.deleteCar(id);
	}

	@PostMapping("/cars/player/{id}")
	public Car addCarToPerson(@Valid @RequestBody Car car, @PathVariable Long id) {

		return carService.attributeCarToPerson(car, id);

	}
	//dodanie i tworzenie nowego auta
	@PostMapping("/cars/{carId}/player/{personId}")
	public Set<Car> addCarToPerson(@RequestBody Car car,@PathVariable Long personId,@PathVariable Long carId) {

		return carService.addToPerson(personId, carId);

	}
	
	
	@GetMapping("/cars/player/{id}")
	public Set<Car> getCarsByPersonId (@PathVariable Long id) {
		
		return carService.getCarsByPersonId(id);
	}
	
	@GetMapping("/cars/{id}")
	public Car getCarById (@PathVariable Long id) {

		return carService.getCarById(id);
	}

	@DeleteMapping("/cars/{carId}/player/{personId}")
	public Set<Car> removeCarFromPersonList (@PathVariable Long personId,@PathVariable Long carId) {

		return carService.removeCarFromPersonList(personId, carId);
	}
	
	
}
