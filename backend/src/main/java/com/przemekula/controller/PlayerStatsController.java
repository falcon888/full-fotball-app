package com.przemekula.controller;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.przemekula.model.Player;
import com.przemekula.model.PlayerStats;
import com.przemekula.service.PlayerStatsService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class PlayerStatsController {

	@Autowired
	private PlayerStatsService playerStatsService;

	@PostMapping("/players/{playerId}/stats")
	public Player addPlayerStats(@PathVariable Long playerId, @Valid @RequestBody PlayerStats playerStats) {
		return playerStatsService.addPlayerStats(playerId, playerStats);
	}
	
	@GetMapping("/players/stats/{playerId}")
	public PlayerStats getStatsByPlayerId(@PathVariable Long playerId) {
		return playerStatsService.getStatsByPlayerId(playerId);
	}

	@PutMapping("/players/{playerId}/stats")
	public Player updatePlayer(@PathVariable Long playerId, @Valid @RequestBody PlayerStats playerStatsUpdated) {
		return playerStatsService.updatePlayer(playerId, playerStatsUpdated);
	}

}
