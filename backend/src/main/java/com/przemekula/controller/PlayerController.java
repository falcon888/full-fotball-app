package com.przemekula.controller;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.przemekula.model.Player;
import com.przemekula.model.dto.PlayerDto;
import com.przemekula.service.PlayerService;


@CrossOrigin(origins = "http://localhost:4200")
@RestControllerAdvice
@RestController
@RequestMapping("/api")
public class PlayerController {

	@Autowired
	private PlayerService playerService;

	@GetMapping("/players")
	public List<PlayerDto> getAllPlayers() {

		return playerService.getAllPlayers();
	}

	@GetMapping("/clubs/{clubId}/players")
	public List<PlayerDto> getPlayersByClubId(@PathVariable Long clubId) {

		return playerService.getPlayersByClubId(clubId);
	}

	@PostMapping("/clubs/{clubId}/players")
	public Player addPlayer(@PathVariable Long clubId, @Valid @RequestBody Player player) {
		return playerService.addPlayer(clubId, player);
	}

	@PutMapping("/clubs/{clubId}/players/{playerId}")
	public Player updatePlayerByClub(@PathVariable Long clubId, @PathVariable Long playerId,
			@Valid @RequestBody Player playerUpdated) {

		return playerService.updatePlayerByClub(clubId, playerId, playerUpdated);
	}
	
	@PutMapping("/players/{playerId}")
	public PlayerDto updatePlayer( @PathVariable Long playerId,
			@Valid @RequestBody PlayerDto playerUpdated) {
		return new PlayerDto(playerService.updatePlayer( playerId, playerUpdated));
	}
	

	@DeleteMapping("/clubs/{clubId}/players/{playerId}")
	public String deletePlayerByClub(@PathVariable Long clubId, @PathVariable Long playerId) {
		return playerService.deletePlayerByClub(clubId, playerId);
	}

	
	@DeleteMapping("/clubs/players/{playerId}")
	public String deletePlayer(@PathVariable Long playerId) {
		return playerService.deletePlayer(playerId);
	}
	
	// bez usuwania statystyk bo to bez sensu usuwac

	
	@GetMapping("/players/{playerName}")
	public List<PlayerDto> getPlayerByName(@PathVariable String playerName) {
		return playerService.getPlayerByName(playerName);
	}

	@GetMapping("/players/id/{id}")
	public PlayerDto getPlayerById(@PathVariable Long id) {
		return new PlayerDto(playerService.getPlayerById(id));
	}
}

