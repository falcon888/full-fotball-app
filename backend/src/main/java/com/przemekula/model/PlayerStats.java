package com.przemekula.model;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "playerStats")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class PlayerStats implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column(name = "matches")
	private int matches;
	
	@Column(name = "goals")
	private int goals;
	
	@Column(name = "yellow_cards")
	private int yellow_cards;
	
	@Column(name = "red_cards")
	private int red_cards;
	
	@Column(name = "height")
	private int height;
	
	@Column(name = "weight")
	private int weight;

	public PlayerStats() {
	}
	
//	@OneToOne(mappedBy = "stats" , cascade = CascadeType.ALL)
//	private Player player;
	  
	public PlayerStats(int matches, int goals, int yellow_cards, int red_cards, int height, int weight) {
		this.matches = matches;
		this.goals = goals;
		this.yellow_cards = yellow_cards;
		this.red_cards = red_cards;
		this.height = height;
		this.weight = weight;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMatches() {
		return matches;
	}

	public void setMatches(int matches) {
		this.matches = matches;
	}

	public int getGoals() {
		return goals;
	}

	public void setGoals(int goals) {
		this.goals = goals;
	}

	public int getYellow_cards() {
		return yellow_cards;
	}

	public void setYellow_cards(int yellow_cards) {
		this.yellow_cards = yellow_cards;
	}

	public int getRed_cards() {
		return red_cards;
	}

	public void setRed_cards(int red_cards) {
		this.red_cards = red_cards;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	
	
}
