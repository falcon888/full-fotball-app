package com.przemekula.model.dto;
import com.przemekula.model.Club;

public class ClubDto {

	private Long id;

	private String name;

	private String stadium;

	private String coach;

	private double budget;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStadium() {
		return stadium;
	}

	public void setStadium(String stadium) {
		this.stadium = stadium;
	}

	public String getCoach() {
		return coach;
	}

	public void setCoach(String coach) {
		this.coach = coach;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public ClubDto() {

	}

	public ClubDto(Club club) {
		this.id = club.getId();
		this.name = club.getName();
		this.stadium = club.getStadium();
		this.coach = club.getCoach();
		this.budget = club.getBudget();
	}

}
