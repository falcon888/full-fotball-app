package com.przemekula.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.przemekula.model.Player;

public class PlayerDto {
	
	private long id;

	
	private String name;


	private String surname;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date dateBirth;

	
	private int marketValue;

	private boolean active;
	
	private Long club_id;

	
	
	public Long getClub_id() {
		return club_id;
	}

	public void setClub_id(Long club_id) {
		this.club_id = club_id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public int getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(int marketValue) {
		this.marketValue = marketValue;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public PlayerDto() {
		super();
	}

public PlayerDto(Player player) {
	
	this.id=player.getId();
	this.name = player.getName();
	this.surname = player.getSurname();
	this.dateBirth = player.getDateBirth();
	this.active = player.isActive();
	this.marketValue = player.getMarketValue();
	this.club_id = player.getClub().getId();
}
}
