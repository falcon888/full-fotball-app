package com.przemekula.model;
//package com.javasampleapproach.springrest.postgresql.model;
//
//import java.util.List;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "national")
//public class National {
//
//	
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	private long id;
//	
//	
//	@Column(name = "name")
//	private String name;
//	
//		
//	@Column(name = "coach")
//	private String coach;
//	
//	@Column(name = "worldCup")
//	private int worldCup;
//	
//	
//	  @OneToMany(mappedBy = "national")
//	    private List<Player> players;
//
//
//	  
//	  
//	public National(String name, String coach, int worldCup, List<Player> players) {
//		super();
//		this.name = name;
//		this.coach = coach;
//		this.worldCup = worldCup;
//		this.players = players;
//	}
//
//
//	public long getId() {
//		return id;
//	}
//
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//
//	public String getName() {
//		return name;
//	}
//
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//
//	public String getCoach() {
//		return coach;
//	}
//
//
//	public void setCoach(String coach) {
//		this.coach = coach;
//	}
//
//
//	public int getWorldCup() {
//		return worldCup;
//	}
//
//
//	public void setWorldCup(int worldCup) {
//		this.worldCup = worldCup;
//	}
//
//
//	public List<Player> getPlayers() {
//		return players;
//	}
//
//
//	public void setPlayers(List<Player> players) {
//		this.players = players;
//	}
//	
//	
//}
