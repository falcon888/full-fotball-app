package com.przemekula.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "cars")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Car {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "brand")
	private String brand;

	@Column(name = "model", unique = true)
	private String model;

	public Car() {
	}

	public Car(String brand, String model) {
		this.brand = brand;
		this.model = model;
	}
//	 @ManyToMany(mappedBy = "cars" , cascade = { CascadeType.ALL })
//	@ManyToMany(mappedBy = "cars" , cascade = { CascadeType.ALL }) <--- usuwajac car usuwa player
	@ManyToMany(mappedBy = "cars") // usuwajac samochodu nie usuwamy uzytkownika !!!!!  
	private Set<Player> players = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", brand=" + brand + ", model=" + model + "]";
	}

}
