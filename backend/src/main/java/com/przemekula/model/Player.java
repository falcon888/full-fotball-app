package com.przemekula.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "players")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Player implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "surname")
	private String surname;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name = "dateBirth")
	private Date dateBirth;

	@Column(name = "marketValue")
	private int marketValue;

	@Column(name = "active")
	private boolean active;

	public Player() {
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "club_id", nullable = false)
	@JsonIgnore
	private Club club;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PlayerStats stats;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "players_cars", joinColumns = { @JoinColumn(name = "players_id") }, inverseJoinColumns = {
			@JoinColumn(name = "cars_id") })
	Set<Car> cars = new HashSet<>();

	public Player(String name, String surname, Date dateBirth, int marketValue, boolean active) {
		this.name = name;
		this.surname = surname;
		this.dateBirth = dateBirth;
		this.marketValue = marketValue;
		this.active = active;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public int getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(int marketValue) {
		this.marketValue = marketValue;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}

	public PlayerStats getStats() {
		return stats;
	}

	public void setStats(PlayerStats stats) {
		this.stats = stats;
	}

	public Set<Car> getCars() {
		return cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", surname=" + surname + ", dateBirth=" + dateBirth
				+ ", marketValue=" + marketValue + ", active=" + active + ", club=" + club + ", stats=" + stats + "]";
	}

}
