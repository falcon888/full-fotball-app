package com.przemekula.dao;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.przemekula.model.Car;

public interface CarDao extends PagingAndSortingRepository<Car, Long> {

}
