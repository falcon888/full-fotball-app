package com.przemekula.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.przemekula.model.Player;

public interface PlayerDao extends JpaRepository<Player, Long> {
	List<Player> findByClubId(Long clubId);	
	List<Player> findBySurname(String surname);
	Player findPlayerById(Long id);
	

}
