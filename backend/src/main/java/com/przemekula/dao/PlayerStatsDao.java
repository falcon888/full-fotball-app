package com.przemekula.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.przemekula.model.PlayerStats;

public interface PlayerStatsDao extends JpaRepository<PlayerStats,Long>{

}
