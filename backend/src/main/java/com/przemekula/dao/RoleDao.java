package com.przemekula.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.przemekula.model.Role;

public interface RoleDao extends JpaRepository<Role, Long>{

	public Role findByName(String name);
	
}
