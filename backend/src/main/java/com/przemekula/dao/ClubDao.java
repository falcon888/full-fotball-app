package com.przemekula.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.przemekula.model.Club;

public interface ClubDao extends JpaRepository<Club, Long> {
}