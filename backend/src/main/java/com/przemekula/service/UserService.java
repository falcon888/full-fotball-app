package com.przemekula.service;

import java.util.List;

import com.przemekula.model.User;
import com.przemekula.model.dto.UserDto;

public interface UserService {

    User save(UserDto user);
    List<User> findAll();
    void delete(long id);
    User findOne(String username);
    User findById(Long id);
    User setUserAuthority(User user);
}