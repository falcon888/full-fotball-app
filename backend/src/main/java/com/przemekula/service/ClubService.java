package com.przemekula.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.boot.autoconfigure.cassandra.ClusterBuilderCustomizer;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.przemekula.model.Club;
import com.przemekula.model.dto.ClubDto;

public interface ClubService {

	public List<ClubDto> getAllClubs();
	public Club getClubById(@PathVariable Long id);
	public Club createClub(@Valid @RequestBody Club club);
	public Club updateClub(@PathVariable Long id, @Valid @RequestBody Club clubUpdated);
	public String deleteClub(@PathVariable Long id);
	public Club transferPlayer(@PathVariable Long clubId, @PathVariable Long playerId, @PathVariable Long newClubId);
	
}
