package com.przemekula.service;
import com.przemekula.model.Player;
import com.przemekula.model.PlayerStats;

public interface PlayerStatsService {

	public Player addPlayerStats(Long playerId, PlayerStats playerStats);

	public Player updatePlayer(Long playerId, PlayerStats playerStatsUpdated);

	public PlayerStats getStatsByPlayerId (Long id);
}
