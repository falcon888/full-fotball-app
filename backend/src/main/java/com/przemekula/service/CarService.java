package com.przemekula.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Sort.Direction;

import com.przemekula.model.Car;

public interface CarService {

	public List<Car> getAllCars(int pages, int size , String sortField , Direction sortOrder);
//	public List<Car> pagi_getAllCars();
	public Set<Car> getCars(Long id);
	public Car createCar(Car car);
	public Car updateCar(Long id, Car updatedCar);
	public String deleteCar(Long id);
	public Car attributeCarToPerson(Car car, Long id);
	public Set<Car> getCarsByPersonId (Long id);
	public Set<Car> addToPerson(Long personId,Long carId);
	public Car getCarById(Long id);
	public Set<Car> removeCarFromPersonList (Long personId , Long carId);
}
