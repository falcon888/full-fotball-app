package com.przemekula.service.impl;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.naming.NameAlreadyBoundException;
import javax.naming.NameNotFoundException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.przemekula.dao.ClubDao;
import com.przemekula.dao.PlayerDao;
import com.przemekula.exception.NotFoundException;
import com.przemekula.model.Club;
import com.przemekula.model.dto.ClubDto;
import com.przemekula.service.ClubService;

@Service
public class ClubServiceImpl implements ClubService {

	@Autowired
	ClubDao clubDao;

	@Autowired
	PlayerDao playerDao;
	
	
	@Override
	public List<ClubDto> getAllClubs() {
		return clubDao.findAll().stream().map(club->
		new ClubDto(club)).collect(Collectors.toList());
	}

	@Override
	public Club getClubById(Long id) {
		Optional<Club> optClub = clubDao.findById(id);
		if (optClub.isPresent()) {
			return optClub.get();
		} else {
			throw new NotFoundException("Club not found with id " + id);
		}
	}

	@Override
	public Club createClub(@Valid Club club) {

		for (Club club2 : clubDao.findAll()) {
			if ((club.getName().equalsIgnoreCase(club2.getName()))
					&& (club.getStadium().equalsIgnoreCase(club2.getStadium())))
				throw new NotFoundException(
						"Club " + club.getName() + " which plays in " + club.getStadium() + " is already exist");
		}

		return clubDao.save(club);
	}

	@Override
	public Club updateClub(Long id, @Valid Club clubUpdated) {
		return clubDao.findById(id).map(club -> {
			club.setName(clubUpdated.getName());
			club.setBudget(clubUpdated.getBudget());
			club.setCoach(clubUpdated.getCoach());
			club.setStadium(clubUpdated.getStadium());
			return clubDao.save(club);
		}).orElseThrow(() -> new NotFoundException("Club not found with id " + id));
	}

	@Override
	public String deleteClub(Long id) {
	
		if(!clubDao.existsById(id)) {
			throw new NotFoundException("Club is not exist");
		}
		
		try {
	clubDao.delete(clubDao.findById(id).get());
			return "Delete Successfully!";
		}
		catch(Exception exception) {
			throw new NotFoundException("Club has many associated ");
		}
		}

	@Override
	public Club transferPlayer(Long clubId, Long playerId, Long newClubId) {
		
		if (( !clubDao.existsById(clubId) || !playerDao.existsById(playerId) || !clubDao.existsById(newClubId) || clubId==newClubId )) {
			throw new NotFoundException("Transfer paramaters are invalid");
		}
		
		if(playerDao.findById(playerId).get().getClub().getId() != clubId) {
			throw new NotFoundException("Club "+clubDao.getOne(clubId).getName()+" don't have "+playerDao.getOne(playerId).getSurname());
		}
	
		if(playerDao.findById(playerId).get().getMarketValue()> clubDao.getOne(newClubId).getBudget()) {
			throw new NotFoundException("The new Club don't have money for this player");
		}
		
		
		
		try {
		clubDao.getOne(clubId).setBudget(clubDao.getOne(clubId).getBudget()+ playerDao.getOne(playerId).getMarketValue());
		clubDao.getOne(newClubId).setBudget(clubDao.getOne(newClubId).getBudget() - playerDao.getOne(playerId).getMarketValue());
		playerDao.getOne(playerId).setClub(clubDao.getOne(newClubId));
		playerDao.save(playerDao.getOne(playerId));
		clubDao.save(clubDao.getOne(clubId));
		clubDao.save(clubDao.getOne(newClubId));
		
		} catch(Exception e) {
			throw new NotFoundException("Some bug in transfer");
		}
		
		
	return clubDao.getOne(newClubId);	
	}

}
