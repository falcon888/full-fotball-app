package com.przemekula.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.method.annotation.RequestParamMapMethodArgumentResolver;

import com.przemekula.dao.CarDao;
import com.przemekula.dao.PlayerDao;
import com.przemekula.exception.NotFoundException;
import com.przemekula.model.Car;
import com.przemekula.model.Player;
import com.przemekula.service.CarService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	CarDao carDao;

	@Autowired
	PlayerDao playerDao;

	@Override
	public List<Car> getAllCars(int pages, int size , String sortField , Direction sortOrder) {
		
		List<Car> carList= new ArrayList<>();
		
Pageable page = PageRequest.of(pages-1, size,new Sort(sortOrder,sortField));

Page<Car> cars = carDao.findAll(page); // iterable pozwala dodac carsy do for-eacha
long dataCount = cars.getTotalElements();
cars.forEach(carList::add);

		return carList;
	}

	@Override
	public Set<Car> getCars(@PathVariable Long id) {
		if (!playerDao.existsById(id)) {
			throw new NotFoundException("Player with id: " + id + " is not exist");
		}

		return playerDao.getOne(id).getCars();
	}

	@Override
	public Car createCar(@Valid @RequestBody Car car) {

//		for (Car car2 : carDao.findAll()) {
//			if ((car2.getBrand().equalsIgnoreCase(car.getBrand()))
//					&& (car2.getModel().equalsIgnoreCase(car.getModel())))
//				throw new NotFoundException("Car " + car.getBrand() + " " + car.getModel() + " is already exist");
//		}

		return carDao.save(car);
	}

	@Override
	public Car updateCar(@PathVariable Long id, @Valid @RequestBody Car carUpdated) {
		return carDao.findById(id).map(car -> {
			car.setBrand(carUpdated.getBrand());
			car.setModel(carUpdated.getModel());
			return carDao.save(car);
		}).orElseThrow(() -> new NotFoundException("Club not found with id " + id));
	}

	@Override
	public String deleteCar(@PathVariable Long id) {
//		return carDao.findById(id).map(car -> {
//			carDao.delete(car);
//			return "Delete Successfully!";
//		}).orElseThrow(() -> new NotFoundException("Car not found with id " + id));
		if(!carDao.existsById(id)) {
			throw new NotFoundException("Car not found with id "+id);
		}
		try {
			carDao.deleteById(id);
		
		} catch (Exception e) {
			throw new NotFoundException("Can't remove a car which user has");
	
		}
		return "Delete car "+id+ " successfully";
	}

	// dodanie ciala auta (tworzenie nowego i przypisanie od razu do uzytkownika)
	@Override
	public Car attributeCarToPerson(@Valid @RequestBody Car car, @PathVariable Long id) {

		if (!playerDao.existsById(id)) {
			throw new NotFoundException("Player not found!");
		}

		for (Car car2 : playerDao.getOne(id).getCars()) {
			if ((car2.getBrand().equals(car.getBrand())) && (car2.getModel().equals(car.getModel())))
				throw new NotFoundException(
						"Car " + car.getBrand() + " " + car.getModel() + " is already in his collection");
		}

		Set<Car> personCars = playerDao.findById(id).get().getCars();
		personCars.add(car);

		playerDao.findById(id).get().setCars(personCars);
		return carDao.save(car);
	}

	@Override
	public Set<Car> getCarsByPersonId(Long id) {

		if (!playerDao.existsById(id)) {
			throw new NotFoundException("Player with id=" + id + " is not found");
		}

		return playerDao.findById(id).get().getCars();
	}

	@Override
	public Set<Car> addToPerson(@PathVariable Long personId, @PathVariable Long carId) {

		if (!(playerDao.existsById(personId) && carDao.existsById(carId)))
			throw new NotFoundException("Impossible add because of invisible person or  invisible car");

		// for(Car car2 : playerDao.getOne(personId).getCars()) {
		// if((car2.getBrand().equals(playerDao.getOne(personId).getCars().iterator().next().getBrand()))&&(car2.getModel().equals(playerDao.getOne(personId).getCars().iterator().next().getModel())))
		// throw new NotFoundException("Car
		// "+playerDao.getOne(personId).getCars().iterator().next().getBrand() + "
		// "+playerDao.getOne(personId).getCars().iterator().next().getModel()+" is
		// already in his collection");
		// }

		// juz zwraca seta wiec nie trzeba dawac warunku (;

		Car car = carDao.findById(carId).get();
		Player player = playerDao.findById(personId).get();
		player.getCars().add(car);

		playerDao.save(player); // Trzeba zapisac w tym DAO ,zeby wypelnila sie tabela players_cars !!!!!

		return player.getCars();

	}

	@Override
	public Car getCarById(Long id) {
		
		if(!carDao.existsById(id)) {
			throw new NotFoundException("Car with id: "+id+ " is not exist");
		}
		
		return carDao.findById(id).get();
	}

	@Override
	public Set<Car> removeCarFromPersonList(Long personId, Long carId) {
		
		if (!(playerDao.existsById(personId) && carDao.existsById(carId)))
			throw new NotFoundException("Impossible add because of invisible person or  invisible car");
	

		playerDao.findById(personId).get().getCars().remove(carDao.findById(carId).get());
	playerDao.save(playerDao.findById(personId).get());
		
		return playerDao.getOne(personId).getCars();
	}

}
