package com.przemekula.service.impl;


import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.przemekula.dao.ClubDao;
import com.przemekula.dao.PlayerDao;
import com.przemekula.exception.NotFoundException;
import com.przemekula.model.Player;
import com.przemekula.model.dto.PlayerDto;
import com.przemekula.service.PlayerService;

@Service
public class PlayerServiceImpl implements PlayerService {

	@Autowired
	private PlayerDao playerDao;

	@Autowired
	private ClubDao clubDao;

	@Override
	public List<PlayerDto> getAllPlayers() {

		return playerDao.findAll().stream().map(player->
		new PlayerDto(player)).collect(Collectors.toList());
	}

	@Override
	public List<PlayerDto> getPlayersByClubId(Long clubId) {
		
		if (!clubDao.existsById(clubId)) {
			throw new NotFoundException("Club not found!");
		}
//		return playerList.stream().map(player->
//		convertToDto(player)).collect(Collectors.toList());
		
		return playerDao.findByClubId(clubId).stream().map(player->
		new PlayerDto(player)).collect(Collectors.toList());
		

	}

	@Override
	public Player addPlayer(Long clubId, Player player) {
		
		for(Player player2 : playerDao.findAll()) {
			if((player2.getName().equalsIgnoreCase(player.getName()))&&(player2.getSurname().equalsIgnoreCase(player.getSurname())))
				throw new NotFoundException("Player "+player.getName() + " "+player.getSurname()+" is already exist");
		}
		
		
		return clubDao.findById(clubId).map(club -> {
			player.setClub(club);
			return playerDao.save(player);
		}).orElseThrow(() -> new NotFoundException("Club not found!"));
	}

	@Override
	public Player updatePlayerByClub (Long clubId, Long playerId, Player playerUpdated) {
		if (!clubDao.existsById(clubId)) {
			throw new NotFoundException("Club not found!");
		}

		return playerDao.findById(playerId).map(player -> {
			player.setName(playerUpdated.getName());
			player.setSurname(playerUpdated.getSurname());
			player.setMarketValue(playerUpdated.getMarketValue());
			player.setActive(playerUpdated.isActive());
			player.setDateBirth(playerUpdated.getDateBirth());
			return playerDao.save(player);
		}).orElseThrow(() -> new NotFoundException("Player not found!"));
	}

	@Override
	public String deletePlayerByClub(Long clubId, Long playerId) {

		if (!clubDao.existsById(clubId)) {
			throw new NotFoundException("Club not found!");
		}

		return playerDao.findById(playerId).map(player -> {
			playerDao.delete(player);
			return "Deleted Successfully!";
		}).orElseThrow(() -> new NotFoundException("Contact not found!"));

	}

	@Override
	public List<PlayerDto> getPlayerByName(String surname) {

		List<Player> playerList = playerDao.findBySurname(surname);

		if (playerList.isEmpty()) {
			throw new NotFoundException("Surname " + surname + " is not found ");
		}

//		return playerList.stream().map(player->
//		convertToDto(player)).collect(Collectors.toList());
		return playerList.stream().map(player->
		new PlayerDto(player)).collect(Collectors.toList());
	}

//	private PlayerDto convertToDto(Player player) {
//		
//		return new PlayerDto(player);
//	}
	
	@Override
	public Player getPlayerById(Long id) {

		if (!playerDao.existsById(id)) {
			throw new NotFoundException("Player with id=" + id + " is not found ");
		}

		return playerDao.findPlayerById(id);
	}

	@Override
	public String deletePlayer(Long playerId) {
		
		if (!playerDao.existsById(playerId)) {
			throw new NotFoundException("Player with id=" + playerId + " is not found ");
		}		
		return playerDao.findById(playerId).map(player -> {
			playerDao.getOne(playerId).getCars().clear();
			playerDao.delete(player);
			return "Deleted Successfully!";
		}).orElseThrow(() -> new NotFoundException("Contact not found!"));
	}

	@Override
	public Player updatePlayer(Long playerId, PlayerDto playerUpdated) {
	
		
		return playerDao.findById(playerId).map(player -> {
			player.setName(playerUpdated.getName());
			player.setSurname(playerUpdated.getSurname());
			player.setMarketValue(playerUpdated.getMarketValue());
			player.setActive(playerUpdated.isActive());
			player.setDateBirth(playerUpdated.getDateBirth());
			player.setClub(clubDao.getOne(playerUpdated.getClub_id()));
			return playerDao.save(player);
		}).orElseThrow(() -> new NotFoundException("Player not found!"));
	}

}
