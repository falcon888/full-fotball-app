package com.przemekula.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.przemekula.dao.PlayerDao;
import com.przemekula.dao.PlayerStatsDao;
import com.przemekula.exception.NotFoundException;
import com.przemekula.model.Player;
import com.przemekula.model.PlayerStats;
import com.przemekula.service.PlayerStatsService;

@Service
public class PlayerStatsServiceImpl implements PlayerStatsService {

	@Autowired
	private PlayerStatsDao playerStatsDao;

	@Autowired
	private PlayerDao playerDao;

	@Override
	public Player addPlayerStats(Long playerId, PlayerStats playerStats) {
		playerStatsDao.save(playerStats);
		return playerDao.findById(playerId).map(player -> {
			player.setStats(playerStats);
			return playerDao.saveAndFlush(player);
		}).orElseThrow(() -> new NotFoundException("Player not found!"));
	}
	
	public PlayerStats getStatsByPlayerId (Long id) {
	return playerDao.getOne(id).getStats();
}
	

	@Override
	public Player updatePlayer(Long playerId, PlayerStats playerStatsUpdated) {
		if (!playerDao.existsById(playerId)) {
			throw new NotFoundException("Player not found!");
		}
		playerStatsDao.save(playerStatsUpdated);
		return playerDao.findById(playerId).map(player -> {
			player.setStats(playerStatsUpdated);
			return playerDao.save(player);
		}).orElseThrow(() -> new NotFoundException("Player not found!"));
	}

}
