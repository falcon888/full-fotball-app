package com.przemekula.service;

import java.util.List;
import com.przemekula.model.Player;
import com.przemekula.model.dto.PlayerDto;

public interface PlayerService {
	public List<PlayerDto> getAllPlayers();

	public List<PlayerDto> getPlayersByClubId( Long clubId);

	public Player addPlayer(Long clubId, Player player);

	public Player updatePlayerByClub(Long clubId, Long playerId, Player playerUpdated);
	
	public Player updatePlayer(Long playerId, PlayerDto playerUpdated);

	public String deletePlayerByClub(Long clubId, Long playerId);
	
	public List<PlayerDto> getPlayerByName(String name);

	public Player getPlayerById(Long id);
	
	public String deletePlayer(Long playerId);


}
