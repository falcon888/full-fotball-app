import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { User } from '../model/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}
returnUser: any;
  private userUrl = 'http://localhost:8080';

  public getUsers(): Observable<any> {
    return this.http.get(this.userUrl + '/users');
  }

  public signUp(user: Object): Observable<any> {
    return this.http.post(this.userUrl + '/signup' , user);
  }

}
