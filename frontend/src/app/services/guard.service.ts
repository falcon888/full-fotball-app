import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '../../../node_modules/@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(
    private authorizeService: AuthService,
    private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

  //   if (this.) {
  //     return true;
  //   }

  //   this.router.navigate(['login']);

  //   return false;

  // }


    if (!this.authorizeService.isUserLoggedIn()) {
      this.router.navigate(['login']);
      return false;
    }

    return true;
  }
}
