import { Club } from './../model/club';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClubService {

  private baseUrl = 'http://localhost:8080/api/clubs';

  constructor(private http: HttpClient) { }
object: any;
//    basicAuthHeaderString = this.loginService.createBasicAuthenticationHttpheader();

//    header = new HttpHeaders({
//   Authorization: this.basicAuthHeaderString
// });

  getAllClubs(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  getClubById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  // createClub(club: Object): Observable<Object> {
  //   return this.http.post(`${this.baseUrl}` + `/create`, club);
  // }

  createClub(club: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` , club);
  }

  updateClub(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteClub(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

transferPlayer(presentClub: number , playerId: number , futureClub: number): Observable<any> {
  return this.http.put(`${this.baseUrl}/${presentClub}/transfer/${playerId}/to/${futureClub}`, this.object);
}

}
