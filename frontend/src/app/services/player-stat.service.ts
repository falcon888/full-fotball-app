import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerStatService {

  private baseUrl = 'http://localhost:8080/api/players';

  constructor(private http: HttpClient) { }

  addPlayerStats(playerId: number, playerStats: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}/${playerId}/stats` , playerStats);
  }

  updatePlayer(playerId: number , value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${playerId}/stats`, value);
  }

  getStatsByPlayerId(playerId: number): Observable<any> {

    return this.http.get(`${this.baseUrl}/stats/${playerId}`);
  }
}
