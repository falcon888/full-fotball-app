import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PARAMETERS } from '../../../node_modules/@angular/core/src/util/decorators';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  private baseUrl = 'http://localhost:8080/api/cars';

  constructor(private http: HttpClient) { }

  // getCars(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}`);
  // }

  getCars(pages: number , size: number, sortField: string, sortOrder: string): Observable<any> {
    const params = new HttpParams().set('pages', pages.toString())
    .set('size', size.toString())
    .set('sortField', sortField.toString())
    .set('sortOrder', sortOrder.toString());

    console.log(params);

    return this.http.get(`${this.baseUrl}`, {params});
  }

  getCarById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }


  createCar(car: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` , car);
  }

  updateCar(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteCar(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  // w ciele przekazane auto
  addCarToPerson(car: Object , personId: number): Observable<Object> {
    return this.http.post(`${this.baseUrl}/player/${personId}` , car);
  }

// post chce obiekt przekazac

  addCarByIdToPerson(personId: number , carId: number ): Observable<Object> {

    return this.http.get(`${this.baseUrl}/${carId}/player/${personId}`);
  }

  getCarsByPersonId(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/player/${id}`);
  }

  attributeCarToPerson(car: Object , personId: number , carId: number): Observable<Object> {

    return this.http.post(`${this.baseUrl}/${carId}/player/${personId}` , car);
  }

  removeCarFromPersonList(personId: number , carId: number): Observable<Object> {

    return this.http.delete(`${this.baseUrl}/${carId}/player/${personId}`);
  }


}
