import { Observable , of, BehaviorSubject} from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

username: string;
  constructor(private http: HttpClient) {
  }

  attemptAuth(ussername: string, password: string): Observable<any> {
    const credentials = {username: ussername, password: password};
    this.username = ussername;
    console.log('attemptAuth :::::::::::::::: ' + this.username) ;
    return this.http.post<any>('http://localhost:8080/token/generate-token', credentials);
  }
  // niezalogowany === FALSE , zalogowany TRUE
  isUserLoggedIn() {
    const user = sessionStorage.getItem('AuthToken');
    // return of(!(user === null));
    return (!(user === null));
  }
getUserName() {
return this.username;
}

}
