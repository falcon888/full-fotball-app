import { Player } from './../model/player';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private baseUrl = 'http://localhost:8080/api';

  constructor(private http: HttpClient) { }

  getAllPlayers(): Observable<any> {
    return this.http.get(`${this.baseUrl}/players`);
  }

  getPlayersByClubId(clubId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/clubs/${clubId}/players/`);
  }

  // createClub(club: Object): Observable<Object> {
  //   return this.http.post(`${this.baseUrl}` + `/create`, club);
  // }

  addPlayer(clubId: number, player: Player): Observable<Object> {
    return this.http.post(`${this.baseUrl}/clubs/${clubId}/players` , player);
  }

  updatePlayerByClub(clubId: number , playerId: number , value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/clubs/${clubId}/players/${playerId}`, value);
  }

  updatePlayer(playerId: number , value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/players/${playerId}`, value);
  }

  deletePlayerByClub(clubId: number , playerId: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/clubs/${clubId}/players/${playerId}`, { responseType: 'text' });
  }


  deletePlayer( playerId: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/clubs/players/${playerId}`, { responseType: 'text' });
  }

getPlayerByName(playerName: string): Observable<Object> {

  return this.http.get(`${this.baseUrl}/players/${playerName}`);
}


getPlayerById(playerId: number): Observable<any> {

  return this.http.get(`${this.baseUrl}/players/id/${playerId}`);
}

}
