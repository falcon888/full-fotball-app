import { Club } from './../model/club';
import { Component, OnInit } from '@angular/core';
import { ClubService } from '../services/club.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '../../../node_modules/@angular/router';
import { PlayerService } from '../services/player.service';
import { Player } from '../model/player';
import { Location } from '@angular/common';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

presentClub: number;
playerId: number;
futureClub: number;
  submitted = false;
  submitted2 = false;
  form: FormGroup;
value: string;
valueSlice: string;
convertNumber: number;
club: Club;
invalidTransfer = false;
clubs: Club[];
players: Player[];
nav: number;
errorMessage = 'Transfer is impossible.';
  constructor( private _location: Location  , private formBuilder: FormBuilder ,
    private clubService: ClubService , private playerService: PlayerService ,  private router: Router) { }

  ngOnInit() {

    this.form = this.formBuilder.group({
      presentClub: ['', [Validators.required]],
      playerId: ['', [Validators.required]],
      futureClub: ['', [Validators.required ]]
    });

this.clubService.getAllClubs().subscribe(
  response => {
  this.clubs = response;
  });

  }

clubSelected (event) {
// console.log();
this.value = (event.target.value);
this.valueSlice = (this.value.substring(this.value.lastIndexOf(':') + 1));
this.convertNumber = Number(this.valueSlice);

console.log(this.value + ' <-event.target.value');
console.log(this.valueSlice);
this.playerService.getPlayersByClubId(this.convertNumber).subscribe(
    response => {
      console.log(response);
    this.players = response;
    }
  );

}
goToPreviousPage() {
  this._location.back(); }


  get f() { return this.form.controls; }

  onSubmit() {
this.presentClub = this.form.controls['presentClub'].value;
this.playerId = this.form.controls['playerId'].value;
this.futureClub = this.form.controls['futureClub'].value;

this.clubService.transferPlayer(this.presentClub , this.playerId , this.futureClub).subscribe(
  response => {
    this.submitted2 = true;
    console.log(response);
  }, error => {
    this.invalidTransfer = true;
        });
  }
 backToClub() {
   this.nav = this.form.controls['futureClub'].value;
   this.router.navigate([`club/details/${this.nav}`]);
 }

}
