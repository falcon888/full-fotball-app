import { User } from './../model/user';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router } from '../../../node_modules/@angular/router';

import { UserService } from '../services/user.service';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {


  form: FormGroup;
submitted = false;
submitted2 = false;
notification: string;
errorMessage = 'Username or e-mail is already use.';
invalidLogin = false;
user: User = new User();

  constructor(private formBuilder: FormBuilder , private _location: Location, private router: Router
    , private userService: UserService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(12)]],
      password: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(12)]],
      email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30), Validators.email]],
      age: ['', [Validators.required, Validators.min(18), Validators.max(100)]]
    });
  }

  get f() { return this.form.controls; }

signUp() {
  this.submitted = true;
  if (this.form.invalid) {
    return;
}

  this.user.username = this.form.controls['username'].value;
  this.user.password = this.form.controls['password'].value;
  this.user.email = this.form.controls['email'].value;
  this.user.age = this.form.controls['age'].value;

  this.userService.signUp(this.user).subscribe(
    response => {
      console.log(response);
      this.submitted2 = true;
      this.user = new User();

    },
    error => {
      console.log(this.user);
      this.invalidLogin = true;
      this.notification = 'Username or e-mail is already use.';
      console.log('Authentication error');
    });
}
backToLogin() {
  this.router.navigate([`login`]);
}

}
