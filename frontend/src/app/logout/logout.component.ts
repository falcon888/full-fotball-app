import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '../../../node_modules/@angular/router';
import { TokenStorage } from '../model/token.storage';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router ,  private token: TokenStorage ,
    private _location: Location , private authorizeService: AuthService) { }

  ngOnInit() {
this.token.signOut();
// this.authorizeService.isUserLoggedIn = false;
  }


  goToLogin(): void {
    this.router.navigate(['login']);
  }

goToPreviousPage(): void {
  this._location.back();
}

}
