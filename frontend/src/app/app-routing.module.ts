import { LogoutComponent } from './logout/logout.component';
import { ModifyPlayerStatComponent } from './playerStat/modify-player-stat/modify-player-stat.component';
import { CarModifyComponent } from './car/car-modify/car-modify.component';
import { CreateClubComponent } from './club/create-club/create-club.component';
import { ClubDetailsComponent } from './club/club-details/club-details.component';
import { CarDetailsComponent } from './car/car-details/car-details.component';
import { CreateCarComponent } from './car/create-car/create-car.component';
import { ClubListComponent } from './club/club-list/club-list.component';
import { CarListComponent } from './car/car-list/car-list.component';
import { ErrorComponent } from './error/error.component';
import { MenuComponent } from './menu/menu.component';
import { SearchPlayersComponent } from './player/search-players/search-players.component';
import { CreatePlayerComponent } from './player/create-player/create-player.component';
import { PlayerListComponent } from './player/player-list/player-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayerStatListComponent } from './playerStat/player-stat-list/player-stat-list.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { GuardService } from './services/guard.service';
import { SingupComponent } from './singup/singup.component';
import { TransferComponent } from './transfer/transfer.component';

const routes: Routes = [
    // { path: '', redirectTo: 'menu', pathMatch: 'full' },
    { path: '', component: LoginComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent, pathMatch: 'full' },
    { path: 'signup', component: SingupComponent},
    { path: 'transfer', component: TransferComponent},
    { path: 'welcome', component: WelcomeComponent, canActivate: [GuardService] },
    { path: 'players', component: PlayerListComponent, canActivate: [GuardService] },
    { path: 'players/stats/:id', component: PlayerStatListComponent, canActivate: [GuardService] },
    { path: 'players/stats/modify/:id', component: ModifyPlayerStatComponent, canActivate: [GuardService] },
    { path: 'clubs', component: ClubListComponent, canActivate: [GuardService]},
    { path: 'club/add', component: CreateClubComponent, canActivate: [GuardService] },
    { path: 'club/add/:id', component: CreateClubComponent, canActivate: [GuardService] },
    { path: 'player/add', component: CreatePlayerComponent, canActivate: [GuardService]},
    { path: 'player/add/:id', component: CreatePlayerComponent, canActivate: [GuardService] },
    { path: 'cars', component: CarListComponent, canActivate: [GuardService] },
    { path: 'cars/player/:id', component: CarDetailsComponent, canActivate: [GuardService] },
    { path: 'cars/add', component: CreateCarComponent, canActivate: [GuardService] },
    { path: 'cars/add/:id', component: CreateCarComponent, canActivate: [GuardService]},
    { path: 'cars/player/:personId/add', component: CarModifyComponent, canActivate: [GuardService] },
    { path: 'addCar', component: CreateCarComponent, canActivate: [GuardService] },
    { path: 'findbysurname', component: SearchPlayersComponent, canActivate: [GuardService] },
    { path: 'club/details/:id', component: ClubDetailsComponent, canActivate: [GuardService] },
    { path: 'user', component: UserComponent, canActivate: [GuardService] },
    { path: 'logout', component: LogoutComponent, canActivate: [GuardService] },
    { path: '**', component: ErrorComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
