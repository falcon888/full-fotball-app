import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '../../../node_modules/@angular/router';
import { TokenStorage } from '../model/token.storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  form: FormGroup;
  submitted  = false;
  notification: string;
  errorMessage = 'Invalid username or password';
  invalidLogin = false;
  constructor(private router: Router, private authService: AuthService, private token: TokenStorage ,
    private formBuilder: FormBuilder , private _location: Location ) {
  }


  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(12)]],
      password: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(12)]],
    });
}

  get f() { return this.form.controls; }


  login(): void {
    this.submitted = true;
    this.username = this.form.controls['username'].value;
    this.password = this.form.controls['password'].value;
    this.authService.attemptAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);
        console.log('Uzytkownik zalogowany ;)');
        this.router.navigate(['welcome']);
        this.invalidLogin = false;
      }
      ,
      error => {
        this.invalidLogin = true;
        this.notification = 'Incorrect username or password.';
        console.log('Authentication error');
      }
    );

  }

  signUp(): void {
    this.router.navigate(['signup']);
  }


logOut(): void {
  this.token.signOut();
  this.router.navigate(['login']);
}
goToPreviousPage(): void {
  this._location.back();
}


}
