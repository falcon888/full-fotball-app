import { CarService } from './../../services/car.service';
import { Component, OnInit } from '@angular/core';
import { Car } from 'src/app/model/car';
import { Router } from '../../../../node_modules/@angular/router';
import { SortEvent, LazyLoadEvent } from 'primeng/components/common/api';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit {

 cars: Car[];
 message: string;
 cols: any[];

  constructor(private carService: CarService , private router: Router) { }

  ngOnInit() {
    this.reloadData();


    this.cols = [
      { field: 'brand', header: 'Brand' },
      { field: 'model', header: 'Model' },
  ];

  }
  reloadData() {
 this.carService.getCars(1, 20, 'brand', '1').subscribe(
   response => {
     console.log(response);
     this.cars = response;
   });
  }
  paginate(event) {
    event.first = 1;
    event.rows = 20;
    event.page = 1;
    event.pageCount = 100;
}
loadCarsLazy(event: LazyLoadEvent) {
console.log(event);
}


  updateCar(id) {
    console.log(`update ${id}`);
    this.router.navigate(['cars/add', id]);
  }
  deleteCar(id) {
    console.log(`delete car ${id}`);
    this.carService.deleteCar(id).subscribe (
      response => {
        console.log(response);
        this.message = `Delete of Car ${id} Successful!`;
        this.reloadData();
      }, error => {alert('Car is associated with player');
  });
}


customSort(event: SortEvent) {
  event.data.sort((data1, data2) => {
      const value1 = data1[event.field];
      const value2 = data2[event.field];
      let result = null;

      if (value1 == null && value2 != null) {
          result = -1;
      } else if (value1 != null && value2 == null) {
          result = 1;
      } else if (value1 == null && value2 == null) {
          result = 0;
       } else if (typeof value1 === 'string' && typeof value2 === 'string') {
          result = value1.localeCompare(value2);
         }  else {
          result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
              }

      return (event.order * result);
  });
}




}
