import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarModifyComponent } from './car-modify.component';

describe('CarModifyComponent', () => {
  let component: CarModifyComponent;
  let fixture: ComponentFixture<CarModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
