import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { PlayerService } from '../../services/player.service';
import { CarService } from '../../services/car.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Car } from '../../model/car';
import { Location } from '@angular/common';

@Component({
  selector: 'app-car-modify',
  templateUrl: './car-modify.component.html',
  styleUrls: ['./car-modify.component.css']
})
export class CarModifyComponent implements OnInit {

  personId: number;
  submitted = false;
  idForm: number;
cars: Car[];
car: Car = new Car();
form: FormGroup;
submitted2 = true;
constructor(private carService: CarService , private route: ActivatedRoute ,
  private _location: Location , private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.personId = this.route.snapshot.params['personId'];
this.reloadData();


this.form = this.formBuilder.group({
  fullCar: ['', [Validators.required , Validators.min(1)]]
});

  }

  get f() { return this.form.controls; }

reloadData() {
this.carService.getCars(7, 6, 'la' , 'la').subscribe(
  response => { console.log(response);
  this.cars = response;
  }
);
}

addNext () {

this.submitted = false;
this.submitted2 = true;
}

onSubmit() {
console.log(this.idForm);

  if (this.form.invalid) {
    return;  }
    this.submitted = true;
    this.idForm = this.form.controls['fullCar'].value;
    this.attributeCarToPerson(this.idForm);
    this.submitted2 = false;
// this.idForm = -1;


}


attributeCarToPerson(carId) {
this.carService.attributeCarToPerson(this.car , this.personId , carId).subscribe(
  response => {
    console.log(response);
    console.log('Succesfully added car to Person');
  }
);
}


  goToPreviousPage() {
this._location.back();
  }


}
