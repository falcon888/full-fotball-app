import { Player } from './../../model/player';
import { PlayerService } from './../../services/player.service';
import { Car } from 'src/app/model/car';
import { CarService } from './../../services/car.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.css']
})
export class CarDetailsComponent implements OnInit {

cars: any;
personId: number;
player: Player;
nameSurname: string;



  constructor(private carService: CarService , private route: ActivatedRoute ,
      private playerService: PlayerService , private _location: Location , private router: Router) { }

  ngOnInit() {

    this.personId = this.route.snapshot.params['id'];

    this.carService.getCarsByPersonId(this.personId).subscribe(
      response => {
        console.log(response);
        console.log('elo');
        console.log(this.personId);
        this.cars = response;
      }
    );
    this.playerService.getPlayerById(this.personId).subscribe(
      response => {
    console.log(response);
    this.player = response;
    this.nameSurname = 'Cars ' + this.player.name + ' ' + this.player.surname;
    console.log('elo2');
    console.log(this.nameSurname);
    });
  }


removeCarFromList(id) {
this.carService.removeCarFromPersonList(this.player.id , id).subscribe(
response => {
  console.log(response);
  this.cars = response;
});
this.reloadData();
}

  goToCreateCar() {
    // this.router.navigate([`cars/player/add/${this.personId}`]); cars/player/:personId/add
    this.router.navigate([`cars/player/${this.personId}/add`]);
  }

  backClicked() {
    this._location.back();
  }


  reloadData() {
    this.carService.getCarsByPersonId(this.personId).subscribe(
      response => {
        console.log(response);
        console.log('elo');
        console.log(this.personId);
        this.cars = response;
      });
    }

}
