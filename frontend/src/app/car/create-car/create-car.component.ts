import { Location } from '@angular/common';
import { CarService } from './../../services/car.service';
import { Car } from './../../model/car';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-car',
  templateUrl: './create-car.component.html',
  styleUrls: ['./create-car.component.css']
})
export class CreateCarComponent implements OnInit {


  id: number;
  car: Car = new Car();
  submitted = false;
  submitted2 = false;
carUpdated: Car;
form: FormGroup;
personId: number;
  constructor(private carService: CarService, private route: ActivatedRoute,
     private router: Router , private _location: Location , private formBuilder: FormBuilder) {
     }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.personId = this.route.snapshot.params['personId'];
if ( this.id > 0) {
this.carService.getCarById(this.id).subscribe(
  response => {
    console.log(response);
    this.car = response;
    this.updateModel();
    this.updateBrand();
  }
);
}

this.form = this.formBuilder.group({
  model: ['', [Validators.required, Validators.minLength(3)]],
  brand: ['', [Validators.required, Validators.minLength(3)]]
});

  }

  get f() { return this.form.controls; }

  updateModel() {
    this.form.controls['model'].setValue(this.car.model);
  }

  updateBrand() {
    this.form.controls['brand'].setValue(this.car.brand);
  }

goToPreviousPage() {
  this._location.back();
}

  newCar(): void {
    this.submitted = false;
    this.car = new Car();
    this.submitted2 = false;
  }

  save() {
    this.car.brand = this.form.controls['brand'].value;
    this.car.model = this.form.controls['model'].value;
    this.carService.createCar(this.car)
      .subscribe(data => console.log(data), error => console.log(error));
      this.submitted2 = true;
    this.car = new Car();
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
  }


if (this.id > 0) {
  this.submitted2 = true;
  this.car.brand = this.form.controls['brand'].value;
  this.car.model = this.form.controls['model'].value;
  this.carService.updateCar(this.id, this.car)
  .subscribe(data => console.log(data), error => console.log(error));
this.car = new Car();
} else if (this.personId > 0) {
  this.submitted2 = true;
  this.car.brand = this.form.controls['brand'].value;
  this.car.model = this.form.controls['model'].value;
this.carService.attributeCarToPerson(this.car, this.personId , this.id )
.subscribe(response =>
console.log(response) , error => console.log(error));
this.car = new Car();

} else {
    this.save();
}
  }
}
