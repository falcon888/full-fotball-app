import { Location } from '@angular/common';
import { PlayerStat } from './../../model/playerStat';
import { Player } from './../../model/player';
import { PlayerStatService } from './../../services/player-stat.service';
import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../../services/player.service';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-player-stat-list',
  templateUrl: './player-stat-list.component.html',
  styleUrls: ['./player-stat-list.component.css']
})
export class PlayerStatListComponent implements OnInit {

id: number;
player: Player = new Player();
playerStatistics: PlayerStat = new PlayerStat();
// nameSurname = this.player.name + ' ' + this.player.surname;

  constructor( private playerStatService: PlayerStatService, private route: ActivatedRoute , private playerService: PlayerService
  , private _location: Location , private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

this.playerStatService.getStatsByPlayerId(this.id).subscribe(
response => {
  if (response === null) {
    this.playerStatistics.matches = 0;
    this.playerStatistics.goals = 0;
    this.playerStatistics.height = 0;
    this.playerStatistics.weight = 0;
    this.playerStatistics.yellow_cards = 0;
    this.playerStatistics.red_cards = 0;

    this.playerStatService.addPlayerStats(this.id , this.playerStatistics).subscribe(
      response2 => {
    console.log('new stats for player');
  console.log(response2);
  });

  } else {
    this.playerStatistics = response;
    console.log(response);
  }
}
);


this.playerService.getPlayerById(this.id).subscribe(
  response => {
    console.log('tu DTO');
console.log(response);
this.player = response;
}
);
 }
  backClicked() {
    this._location.back();
  }
  // players/stats/modify/:id
  modifyStats() {
    this.router.navigate([`players/stats/modify`, this.id]);
  }
}
