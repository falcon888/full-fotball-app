import { PlayerStat } from './../../model/playerStat';
import { PlayerStatService } from './../../services/player-stat.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-modify-player-stat',
  templateUrl: './modify-player-stat.component.html',
  styleUrls: ['./modify-player-stat.component.css']
})
export class ModifyPlayerStatComponent implements OnInit {

playerId: number;
stats: PlayerStat;
submitted = false;
submitted2 = false;
form: FormGroup;
  constructor(private route: ActivatedRoute , private router: Router ,
  private playerStatService: PlayerStatService ,  private formBuilder: FormBuilder , private _location: Location  ) { }

  ngOnInit() {
    this.playerId = this.route.snapshot.params['id'];


    this.playerStatService.getStatsByPlayerId(this.playerId).subscribe(
      response => {
        console.log(response);
      this.stats = response;
      this.updateMatches();
      this.updateGoals();
      this.updateHeight();
      this.updateWeight();
      this.updateYellowCards();
      this.updateRedCards();
      } );



    this.form = this.formBuilder.group({
      matches: ['', [Validators.required, Validators.min(0) , Validators.max(999)]],
      goals: ['', [Validators.required, Validators.min(0) , Validators.max(999)]],
      height: ['', [Validators.required, Validators.min(140) , Validators.max(999) ]],
      weight: ['', [Validators.required, Validators.min(50), Validators.max(999)]],
      yellow_cards: ['', [Validators.required, Validators.min(0) , Validators.max(999)]],
      red_cards: ['', [Validators.required, Validators.min(0) , Validators.max(999)]]
    });
  }
  get f() { return this.form.controls; }

  updateMatches() {
    this.form.controls['matches'].setValue(this.stats.matches);
  }
  updateGoals() {
    this.form.controls['goals'].setValue(this.stats.goals);
  }
  updateHeight() {
    this.form.controls['height'].setValue(this.stats.height);
  }
  updateWeight() {
    this.form.controls['weight'].setValue(this.stats.weight);
  }
  updateYellowCards() {
    this.form.controls['yellow_cards'].setValue(this.stats.yellow_cards);
  }
  updateRedCards() {
    this.form.controls['red_cards'].setValue(this.stats.red_cards);
  }
  goToPreviousPage() {
    this._location.back(); }



    save() {
      this.stats.matches = this.form.controls['matches'].value;
      this.stats.goals = this.form.controls['goals'].value;
      this.stats.height = this.form.controls['height'].value;
      this.stats.weight = this.form.controls['weight'].value;
      this.stats.yellow_cards = this.form.controls['yellow_cards'].value;
      this.stats.red_cards = this.form.controls['red_cards'].value;

this.playerStatService.updatePlayer(this.playerId , this.stats).subscribe(
  response => {
    console.log(response);
    this.stats = new PlayerStat;
    this.submitted2 = true;
  }

);

 }


  onSubmit() {

    this.submitted = true;
   if (this.form.invalid) {
      return;
  }
    this.save();

  }


  navigateToPlayer() {
    this.router.navigate([`players/stats/${this.playerId}`]);
    this.submitted = false;
    this.submitted2 = false;
  }

}
