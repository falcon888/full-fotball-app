import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyPlayerStatComponent } from './modify-player-stat.component';

describe('ModifyPlayerStatComponent', () => {
  let component: ModifyPlayerStatComponent;
  let fixture: ComponentFixture<ModifyPlayerStatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyPlayerStatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyPlayerStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
