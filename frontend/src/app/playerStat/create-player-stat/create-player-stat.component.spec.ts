import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePlayerStatComponent } from './create-player-stat.component';

describe('CreatePlayerStatComponent', () => {
  let component: CreatePlayerStatComponent;
  let fixture: ComponentFixture<CreatePlayerStatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePlayerStatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePlayerStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
