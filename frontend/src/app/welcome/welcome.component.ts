import { Component, OnInit } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';
import { AuthService } from '../services/auth.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(private router: Router , private _location: Location) { }

  ngOnInit() {

  }
  goToCreateCar() {
    this.router.navigate([`cars/add`]);
  }

  goToCreateClub() {
    this.router.navigate([`club/add`]);
  }

  goToCreatePlayer() {
    this.router.navigate([`player/add`]);
  }

  goToPreviousPage() {
this._location.back();
  }

}
