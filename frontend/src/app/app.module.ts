import { ButtonModule } from 'primeng/button';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { SearchCustomersComponent } from './search-customers/search-customers.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PlayerListComponent } from './player/player-list/player-list.component';
import { CreatePlayerComponent } from './player/create-player/create-player.component';
import { PlayerDetailsComponent } from './player/player-details/player-details.component';
import { SearchPlayersComponent } from './player/search-players/search-players.component';
import { MenuComponent } from './menu/menu.component';
import { ErrorComponent } from './error/error.component';
import { CarListComponent } from './car/car-list/car-list.component';
import { CreateCarComponent } from './car/create-car/create-car.component';
import { CarDetailsComponent } from './car/car-details/car-details.component';
import { SearchCarsComponent } from './car/search-cars/search-cars.component';
import { ClubListComponent } from './club/club-list/club-list.component';
import { ClubDetailsComponent } from './club/club-details/club-details.component';
import { CreateClubComponent } from './club/create-club/create-club.component';
import { SearchClubsComponent } from './club/search-clubs/search-clubs.component';
import { PlayerStatListComponent } from './playerStat/player-stat-list/player-stat-list.component';
import { CreatePlayerStatComponent } from './playerStat/create-player-stat/create-player-stat.component';
import { CarModifyComponent } from './car/car-modify/car-modify.component';
import { ModifyPlayerStatComponent } from './playerStat/modify-player-stat/modify-player-stat.component';
import { Interceptor } from './interceptor';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { TokenStorage } from './model/token.storage';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { LogoutComponent } from './logout/logout.component';
import { GuardComponent } from './guard/guard.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { SingupComponent } from './singup/singup.component';
import { TransferComponent } from './transfer/transfer.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {TableModule} from 'primeng/table';
import { PanelModule} from 'primeng/panel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {PaginatorModule} from 'primeng/paginator';
@NgModule({
  declarations: [
    AppComponent,
    CreateCustomerComponent,
    CustomerDetailsComponent,
    CustomersListComponent,
    SearchCustomersComponent,
    PlayerListComponent,
    CreatePlayerComponent,
    PlayerDetailsComponent,
    SearchPlayersComponent,
    MenuComponent,
    ErrorComponent,
    CarListComponent,
    CreateCarComponent,
    CarDetailsComponent,
    SearchCarsComponent,
    ClubListComponent,
    ClubDetailsComponent,
    CreateClubComponent,
    SearchClubsComponent,
    PlayerStatListComponent,
    CreatePlayerStatComponent,
    CarModifyComponent,
    ModifyPlayerStatComponent,
    UserComponent,
    LoginComponent,
    LogoutComponent,
    GuardComponent,
    WelcomeComponent,
    SingupComponent,
    TransferComponent
  ],
  imports: [PanelModule,
    PaginatorModule,
    BrowserAnimationsModule,
    TableModule,
    ButtonModule,
    AngularFontAwesomeModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],

  providers: [[ErrorDialogComponent, UserService, AuthService, TokenStorage ,
    {provide: HTTP_INTERCEPTORS,
    useClass: Interceptor,
    multi : true}
  ]],
  bootstrap: [AppComponent]
})
export class AppModule {

}
