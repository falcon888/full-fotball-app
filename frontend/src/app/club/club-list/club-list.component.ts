import { ClubService } from './../../services/club.service';
import { Club } from './../../model/club';
import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-club-list',
  templateUrl: './club-list.component.html',
  styleUrls: ['./club-list.component.css']
})
export class ClubListComponent implements OnInit {

  clubs: Club[];
  message: string;


  constructor(private clubService: ClubService , private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
 this.clubService.getAllClubs().subscribe(
   response => {
     console.log(response);
     this.clubs = response;
   });
  }

  showPlayers(id) {
    this.router.navigate([`club/details`, id]);
  }

  updateClub(id) {
    console.log(`update ${id}`);
    this.router.navigate(['club/add', id]);
  }


  deleteClub(id) {
    this.clubService.deleteClub(id).subscribe (
      response => {
        console.log(`delete club ${id}`);
        console.log(response);
        this.message = `Delete of Club ${id} Successful!`;
        this.reloadData();
      },
        error => {alert('Club has many associations');
      }
    );
  }

}
