import { Club } from './../../model/club';
import { Player } from './../../model/player';
import { PlayerService } from './../../services/player.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import {Location} from '@angular/common';
import { ClubService } from '../../services/club.service';

@Component({
  selector: 'app-club-details',
  templateUrl: './club-details.component.html',
  styleUrls: ['./club-details.component.css']
})
export class ClubDetailsComponent implements OnInit {

clubId: number;

players: Player[];

club: Club = new Club();



  constructor(private playerService: PlayerService ,  private route: ActivatedRoute ,
     private _location: Location , private clubService: ClubService , private router: Router) { }

  ngOnInit() {
    this.clubId = this.route.snapshot.params['id'];

    this.playerService.getPlayersByClubId(this.clubId).subscribe(
      response => {
      console.log(response);
      this.players = response;
      });

  this.clubService.getClubById(this.clubId).subscribe(
    response => {
      console.log(response);
      this.club = response;
    });

    }

    goToCreatePlayer() {
      this.router.navigate([`player/add`]);
    }

  backClicked() {
    this._location.back();
  }

}
