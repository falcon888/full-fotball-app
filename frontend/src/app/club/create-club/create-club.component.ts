import { Location } from '@angular/common';
import { Club } from './../../model/club';
import { Component, OnInit } from '@angular/core';
import { ClubService } from '../../services/club.service';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-club',
  templateUrl: './create-club.component.html',
  styleUrls: ['./create-club.component.css']
})
export class CreateClubComponent implements OnInit {

id: number;
club: Club = new Club();
submitted = false;
submitted2 = false;
form: FormGroup;

  constructor(private clubService: ClubService , private _location: Location ,
     private route: ActivatedRoute , private formBuilder: FormBuilder) { }

  ngOnInit() {
this.id = this.route.snapshot.params['id'];
if ( this.id > 0) {
  this.clubService.getClubById(this.id).subscribe(
    response => {
      console.log(response);
      this.club = response;
      this.updateName();
    this.updateStadium();
    this.updateCoach();
    this.updateBudget();
    });
  }

  this.form = this.formBuilder.group({
    name: ['', [Validators.required, Validators.minLength(6)]],
    stadium: ['', [Validators.required, Validators.minLength(6)]],
    coach: ['', [Validators.required, Validators.minLength(6)]],
    budget: ['', [Validators.required , Validators.min(1), Validators.max(999)]]
  });


  }

  get f() { return this.form.controls; }


  updateName() {
    this.form.controls['name'].setValue(this.club.name);
  }

  updateStadium() {
    this.form.controls['stadium'].setValue(this.club.stadium);
  }
  updateCoach() {
    this.form.controls['coach'].setValue(this.club.coach);
  }

  updateBudget() {
    this.form.controls['budget'].setValue(this.club.budget);
  }




  goToPreviousPage() {
    this._location.back();
  }

newClub(): void {
this.submitted = false;
this.club = new Club();
this.submitted2 = false;
}

save() {
  this.club.name = this.form.controls['name'].value;
  this.club.stadium = this.form.controls['stadium'].value;
  this.club.coach = this.form.controls['coach'].value;
  this.club.budget = this.form.controls['budget'].value;
  this.clubService.createClub(this.club).subscribe(
    data => console.log(data), error => console.log(error));
    this.submitted2 = true;
  this.club = new Club();
}

onSubmit() {

  this.submitted = true;
  if (this.form.invalid) {
    return;
}

  if (this.id > 0) {
    this.submitted2 = true;
    this.club.name = this.form.controls['name'].value;
    this.club.stadium = this.form.controls['stadium'].value;
    this.club.coach = this.form.controls['coach'].value;
    this.club.budget = this.form.controls['budget'].value;

this.clubService.updateClub(this.id , this.club) .subscribe(
  data => console.log(data), error => console.log(error));
this.club = new Club();
  } else {
  this.save();
  }
}

}
