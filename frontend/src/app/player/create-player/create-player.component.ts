import { PlayerStatService } from './../../services/player-stat.service';
import { PlayerStat } from './../../model/playerStat';
import { ClubService } from './../../services/club.service';
import { Player } from './../../model/player';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { Location } from '@angular/common';
import { PlayerService } from '../../services/player.service';
import { Club } from '../../model/club';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-create-player',
  templateUrl: './create-player.component.html',
  styleUrls: ['./create-player.component.css']
})
export class CreatePlayerComponent implements OnInit {
  player: Player = new Player();
  playerStat: PlayerStat = new PlayerStat();
  submitted =  false;
clubs: Club[];
club: Club;
errorMessage = 'Username or e-mail is already use.';
id: number;
submitted2 = false;
form: FormGroup;
notification = 'Form is invalid or user is exist';
invalidCreating = false;
  constructor( private _location: Location , private route: ActivatedRoute,
     private clubService: ClubService , private playerService: PlayerService ,
      private playerStatService: PlayerStatService , private router: Router
   , private formBuilder: FormBuilder ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    if ( this.id > 0) {
      this.playerService.getPlayerById(this.id).subscribe(
        response => {
          console.log(response);
          this.player = response;
          this.updateName();
          this.updateSurname();
          this.updateDateBirth();
          this.updateMarketValue();
          this.updateActive();
          this.updateClubId();
        }
      );
    }

    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3) , Validators.maxLength(20)]],
      surname: ['', [Validators.required, Validators.minLength(3) , Validators.maxLength(20)]],
      dateBirth: ['', [Validators.required]],
      marketValue: ['', [Validators.required, Validators.min(1), Validators.max(999)]],
      active: ['', [Validators.required]],
      club_id: ['', [Validators.required]]
    });



  this.clubService.getAllClubs().subscribe(
      response => {
        console.log(this.clubs);
this.clubs = response;
      });
  }
  get f() { return this.form.controls; }

  updateName() {
    this.form.controls['name'].setValue(this.player.name);
  }

  updateSurname() {
    this.form.controls['surname'].setValue(this.player.surname);
  }
  updateDateBirth() {
    this.form.controls['dateBirth'].setValue(this.player.dateBirth);
  }

  updateMarketValue() {
    this.form.controls['marketValue'].setValue(this.player.marketValue);
  }
  updateActive() {
    this.form.controls['active'].setValue(this.player.active);
  }

  updateClubId() {
    this.form.controls['club_id'].setValue(this.player.club_id);
  }



  goToPreviousPage() {
    this._location.back(); }

    save() {
      this.player.name = this.form.controls['name'].value;
      this.player.surname = this.form.controls['surname'].value;
      this.player.dateBirth = this.form.controls['dateBirth'].value;
      this.player.marketValue = this.form.controls['marketValue'].value;
      this.player.active = this.form.controls['active'].value;
      this.player.club_id = this.form.controls['club_id'].value;

    this.playerService.addPlayer(this.player.club_id, this.player).subscribe(
      data => {
      console.log(data);
      this.submitted2 = true;
      this.player = new Player();
      } , error => {
        this.invalidCreating = true;
        console.log('error in creating player');

      });
 }

  newPlayer(): void {
this.submitted = false;
// this.router.navigate([`player/add`]);
this.player = new Player();
this.submitted2 = false;
  }

  onSubmit() {

    this.submitted = true;
    if (this.form.invalid) {
      return;
  }

    if (this.id > 0) {
      this.submitted2 = true;
      this.player.name = this.form.controls['name'].value;
      this.player.surname = this.form.controls['surname'].value;
      this.player.dateBirth = this.form.controls['dateBirth'].value;
      this.player.marketValue = this.form.controls['marketValue'].value;
      this.player.active = this.form.controls['active'].value;
      this.player.club_id = this.form.controls['club_id'].value;

    this.playerService.updatePlayer(this.id , this.player).subscribe(
      data => console.log(data), error => console.log(error));
    } else {
    this.save(); }
  }
}



