
import { Observable } from 'rxjs';
import { PlayerService } from './../../services/player.service';
import { Component, OnInit } from '@angular/core';
import { Player } from 'src/app/model/player';
import { Router } from '../../../../node_modules/@angular/router';
@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {

players: Player[];
message: string;
values: string[];
  constructor(private playerService: PlayerService , private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
 this.playerService.getAllPlayers().subscribe(
   response => {
     console.log(response);
     this.players = response;
   });
  }

showStats(id) {
  this.router.navigate([`players/stats`, id]);
}

showCars(id) {
  this.router.navigate([`cars/player`, id]);
}
updatePlayer(id) {
  console.log(`update ${id}`);
  this.router.navigate(['player/add', id]);
}


deletePlayer(id) {
  console.log(`delete club ${id}`);
  this.playerService.deletePlayer(id).subscribe (
    response => {
      console.log(response);
      this.message = `Delete of Club ${id} Successful!`;
      this.reloadData();
    }
  );
}

}
