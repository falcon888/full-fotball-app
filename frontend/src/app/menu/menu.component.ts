import { AuthService } from './../services/auth.service';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

var: string;
  constructor(private router: Router , private _location: Location , private authorizeService: AuthService) { }

  ngOnInit() {
this.var = '';
}

getName() {
  if (this.authorizeService.isUserLoggedIn()) {
  return this.authorizeService.getUserName();
  } else {
    return this.var;
  }
}

}
