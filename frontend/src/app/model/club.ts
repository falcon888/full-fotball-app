import { Player } from './player';

export class Club {
  id: number;
   name: string;
   stadium: string;
   coach: string;
   budget: number;
   players: Array<Player>;
}
