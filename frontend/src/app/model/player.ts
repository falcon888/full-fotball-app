export class Player {
  id: number;
  name: string;
  surname: string;
  dateBirth: Date;
  marketValue: number;
  active: boolean;
  club_id: number;
}
